# Jogo de Dama

O jogo de damas pratica-se entre dois jogadores. Em um tabuleiro quadrado, de 64 casas alternadamente claras e escuras, dispondo de 12 peças brancas e 12 pretas.

O objetivo é capturar ou imobilizar as peças do adversário. O jogador que conseguir capturar todas as peças do inimigo ganha a partida.

### Como Colocar o aplicativo para funcionar no ambiente local

O Jogo foi desenvolvido em Javascript usando a biblioteca ReactJS

#### Requisitos para o funcionamento
* node e npm: https://nodejs.org
* git: https://git-scm.com/

#### Instalação
```bash
git clone https://gitlab.com/Dadayass/jogoDama.git
cd jogoDama
npm install
```
#### Executar o aplicativo
```console
npm start
``` 
### Acessando o aplicativo na web
* http://jogodama.chas.inf.br/

### Como Jogar

Este Game pode ser jogado por uma ou duas pessoas. O(s) jogador(es) deve(m) selecionar uma das opções: Um Jogador ou Dois Jogadores

O Game começa com o primeiro jogador selecionando a peça na parte de cima do tabuleiro. Uma vez selecionada, vai mostrar no tabuleiro o local em vermelho e o usuário deve selecionar um desses locais para que a pedra seja movimentada.

Logo em seguida é a vez do segundo jogador, que fica na parte de baixo do tabuleiro.

A área de texto em cima do tabuleiro indica quem vai fazer a jogada, mostra quem ganhou e outras mensagens.

Na parte de baixo do tabuleiro, apresenta o botão Desfazer, que vai voltar a última jogada.

Para iniciar um novo jogo, selecione a tecla F5 ou Ctrl+R.

### Créditos

* Fonte: Gabriel Mioni https://github.com/GabrielMioni/react-checkers
* Adaptação e melhorias: Dayane Amaral - dayane.amaral@sempreceub.com & Pedro Henrique Figueiredo - pedro.hf@sempreceub.com