import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Game} from './Game.js';
// import 'bootstrap/dist/css/bootstrap.min.css';

// ========================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);